/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.service;

import com.dhfr.data.core.service.IService;
import com.dhfr.jasamedika.entity.Province;
import java.util.List;

/**
 *
 * @author denifahri
 */
public interface ProvinceService extends IService<Province> {

    public List<Province> getByprovinceName(String provinceName) throws Exception;

    public List<Province> getByContryId(Long countryId) throws Exception;
}
