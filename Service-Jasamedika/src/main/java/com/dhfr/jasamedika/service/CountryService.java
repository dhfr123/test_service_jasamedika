/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.service;

import com.dhfr.data.core.service.IService;
import com.dhfr.jasamedika.entity.Country;
import java.util.List;

/**
 *
 * @author denifahri
 */
public interface CountryService extends IService<Country> {

    public List<Country> getByCountryName(String name) throws Exception;
}
