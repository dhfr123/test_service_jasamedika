/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.service.impl;

import com.dhfr.common.util.RandomNumberUtil;
import com.dhfr.data.core.service.impl.IServiceImpl;
import com.dhfr.jasamedika.CommonConstant;
import com.dhfr.jasamedika.dao.KelurahanDao;
import com.dhfr.jasamedika.dao.PasienDao;
import com.dhfr.jasamedika.entity.Kelurahan;
import com.dhfr.jasamedika.entity.Pasien;
import com.dhfr.jasamedika.exception.BussinessException;
import com.dhfr.jasamedika.service.PasienService;
import com.dhfr.jasamedika.util.KodefikasiUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author denifahri
 */
@Service(value = "PasienService")
@Lazy
public class PasienServiceImpl extends IServiceImpl implements PasienService {

    @Autowired
    private PasienDao pasienDao;
    @Autowired
    private KelurahanDao kelurahanDao;

    @Override
    public Pasien getEntiyByPK(String id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien getEntiyByPK(Integer id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien getEntiyByPK(Long id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void save(Pasien entity) throws Exception {
        doSave(entity);
    }

    @Override
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void update(Pasien entity) throws Exception {
        Pasien pasien = pasienDao.getEntiyByPK(entity.getId());
        Kelurahan kelurahan = kelurahanDao.getEntiyByPK(entity.getKelurahan().getId());
//       Asusmsi  pasienId tidak bisa di edit
        pasien.setKelurahan(kelurahan);
        pasien.setAlamat(entity.getAlamat());
        pasien.setGender(entity.getGender());
        pasien.setName(entity.getName());
        pasien.setPhoneNumber(entity.getPhoneNumber());
        pasien.setRt(entity.getRt());
        pasien.setRw(entity.getRw());
        pasien.setTglLahir(entity.getTglLahir());
        pasien.setUpdatedBy(entity.getUpdatedBy());
        pasien.setUpdatedOn(new Date());
        pasienDao.update(pasien);
    }

    @Override
    public void saveOrUpdate(Pasien enntity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien saveData(Pasien entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien updateData(Pasien entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien saveOrUpdateData(Pasien entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien getEntityByPkIsActive(String id, Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien getEntityByPkIsActive(String id, Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien getEntityByPkIsActive(String id, Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien getEntityByPkIsActive(Integer id, Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien getEntityByPkIsActive(Integer id, Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien getEntityByPkIsActive(Integer id, Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien getEntityByPkIsActive(Long id, Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien getEntityByPkIsActive(Long id, Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pasien getEntityByPkIsActive(Long id, Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete(Pasien entity) throws Exception {
        pasienDao.delete(entity);
    }

    @Override
    public void softDelete(Pasien entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, timeout = 30)
    public Long getTotalData() throws Exception {
        return pasienDao.getTotalData();
    }

    @Override
    public Long getTotalDataIsActive(Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Long getTotalDataIsActive(Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Long getTotalDataIsActive(Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, timeout = 50)
    public List<Pasien> getAllData() throws Exception {
        return this.pasienDao.getAllData();
    }

    @Override
    public List<Pasien> getAllData(Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Pasien> getAllData(Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Pasien> getAllData(Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, timeout = 50)
    public List<Pasien> getAllDataPageAble(int firstResult, int maxResults, Order order) throws Exception {
        return pasienDao.getAllDataPageAble(firstResult, maxResults, order);
    }

    @Override
    public List<Pasien> getAllDataPageAbleIsActive(int firstResult, int maxResults, Order order, Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Pasien> getAllDataPageAbleIsActive(int firstResult, int maxResults, Order order, Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Pasien> getAllDataPageAbleIsActive(int firstResult, int maxResults, Order order, Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, timeout = 30)
    public Pasien getByPasienIdWithDetail(String pasienId) throws Exception {
        return this.pasienDao.getByPasienIdWithDetail(pasienId);
    }

    @Override
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Pasien registrasi(Pasien pasien) throws Exception {
        doSave(pasien);
        return this.pasienDao.getByPasienIdWithDetail(pasien.getPasienId());
    }

    private void doSave(Pasien entity) throws Exception {
        long totalDuplicate = pasienDao.getTotalByPasienId(entity.getPasienId());
        if (totalDuplicate > 0) {
            throw new BussinessException("entity.error_code");
        }

        Pasien currentMax = pasienDao.getByMaxCreateDate();
        Integer lastPasienNumber;
        if (currentMax == null) {
            lastPasienNumber = 0;
        } else {
            lastPasienNumber = KodefikasiUtil.getMaxNumber(currentMax.getPasienId(), CommonConstant.KODEFIKASI_PASIEN);
        }

        Kelurahan kelurahan = kelurahanDao.getEntiyByPK(entity.getKelurahan().getId());
        String pasienId = KodefikasiUtil.getKodefikasi(lastPasienNumber, CommonConstant.KODEFIKASI_PASIEN);
        entity.setId(Long.parseLong(RandomNumberUtil.getRandomNumber(9)));
        entity.setCreatedOn(new Date());
        entity.setKelurahan(kelurahan);
        entity.setPasienId(pasienId);
        pasienDao.save(entity);
    }

}
