/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.service;

import com.dhfr.data.core.service.IService;
import com.dhfr.jasamedika.entity.SpiUserRole;

/**
 *
 * @author denifahri
 */
public interface SpiUserRoleService extends IService<SpiUserRole>{
    
}
