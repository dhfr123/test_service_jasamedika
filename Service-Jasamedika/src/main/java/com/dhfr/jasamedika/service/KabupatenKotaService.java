/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.service;

import com.dhfr.data.core.service.IService;
import com.dhfr.jasamedika.entity.KabupatenKota;
import java.util.List;

/**
 *
 * @author denifahri
 */
public interface KabupatenKotaService extends IService<KabupatenKota> {

    public List<KabupatenKota> getByName(String kabupatenName) throws Exception;

    public List<KabupatenKota> getByProvinceId(Long proviceId) throws Exception;

}
