/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.service;

import com.dhfr.data.core.service.IService;
import com.dhfr.jasamedika.entity.Kecamatan;
import java.util.List;

/**
 *
 * @author denifahri
 */
public interface KecamatanService extends IService<Kecamatan> {

    public List<Kecamatan> getByName(String name) throws Exception;

    public List<Kecamatan> getByKabupatenId(long kabupatenId) throws Exception;
}
