/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.service.impl;

import com.dhfr.common.util.RandomNumberUtil;
import com.dhfr.data.core.service.impl.IServiceImpl;
import com.dhfr.jasamedika.dao.KabupatenKotaDao;
import com.dhfr.jasamedika.dao.KecamatanDao;
import com.dhfr.jasamedika.entity.KabupatenKota;
import com.dhfr.jasamedika.entity.Kecamatan;
import com.dhfr.jasamedika.exception.BussinessException;
import com.dhfr.jasamedika.service.KecamatanService;
import java.util.Date;
import java.util.List;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author denifahri
 */
@Service(value = "kecamatanService")
@Lazy
public class KecamatanServiceImpl extends IServiceImpl implements KecamatanService {

    @Autowired
    private KecamatanDao kecamatanDao;
    @Autowired
    private KabupatenKotaDao kabupatenKotaDao;

    @Override
    public Kecamatan getEntiyByPK(String id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan getEntiyByPK(Integer id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.SUPPORTS, timeout = 50)
    public Kecamatan getEntiyByPK(Long id) throws Exception {
        return kecamatanDao.getEntiyByPK(id);
    }

    @Override
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void save(Kecamatan entity) throws Exception {
        long totalDuplicate = kecamatanDao.getTotalByCode(entity.getCode());
        if (totalDuplicate > 0) {
            throw new BussinessException("entity.error_code");
        }

        KabupatenKota kabupatenKota = kabupatenKotaDao.getEntiyByPK(entity.getKabupatenKota().getId());
        entity.setId(Long.parseLong(RandomNumberUtil.getRandomNumber(9)));
        entity.setCreatedOn(new Date());
        entity.setKabupatenKota(kabupatenKota);
        kecamatanDao.save(entity);
    }

    @Override
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void update(Kecamatan entity) throws Exception {
        long totalDuplicate = kecamatanDao.getTotalByCodeAndNotId(entity.getCode(), entity.getId());
        if (totalDuplicate > 0) {
            throw new BussinessException("entity.error_code");
        }
        KabupatenKota kabupatenKota = kabupatenKotaDao.getEntiyByPK(entity.getKabupatenKota().getId());
        Kecamatan kecamatan = kecamatanDao.getEntiyByPK(entity.getId());
        kecamatan.setCode(entity.getCode());
        kecamatan.setDescription(entity.getDescription());
        kecamatan.setKabupatenKota(kabupatenKota);
        kecamatan.setName(entity.getName());
        kecamatan.setUpdatedBy(entity.getUpdatedBy());
        kecamatan.setUpdatedOn(new Date());
        kecamatanDao.update(kecamatan);
    }

    @Override
    public void saveOrUpdate(Kecamatan enntity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan saveData(Kecamatan entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan updateData(Kecamatan entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan saveOrUpdateData(Kecamatan entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan getEntityByPkIsActive(String id, Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan getEntityByPkIsActive(String id, Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan getEntityByPkIsActive(String id, Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan getEntityByPkIsActive(Integer id, Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan getEntityByPkIsActive(Integer id, Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan getEntityByPkIsActive(Integer id, Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan getEntityByPkIsActive(Long id, Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan getEntityByPkIsActive(Long id, Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Kecamatan getEntityByPkIsActive(Long id, Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete(Kecamatan entity) throws Exception {
        this.kecamatanDao.delete(entity);
    }

    @Override
    public void softDelete(Kecamatan entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, timeout = 30)
    public Long getTotalData() throws Exception {
        return kecamatanDao.getTotalData();
    }

    @Override
    public Long getTotalDataIsActive(Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Long getTotalDataIsActive(Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Long getTotalDataIsActive(Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, timeout = 50)
    public List<Kecamatan> getAllData() throws Exception {
        return kecamatanDao.getAllData();
    }

    @Override
    public List<Kecamatan> getAllData(Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Kecamatan> getAllData(Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Kecamatan> getAllData(Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, timeout = 50)
    public List<Kecamatan> getAllDataPageAble(int firstResult, int maxResults, Order order) throws Exception {
      return kecamatanDao.getAllDataPageAble(firstResult, maxResults, order);
    }

    @Override
    public List<Kecamatan> getAllDataPageAbleIsActive(int firstResult, int maxResults, Order order, Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Kecamatan> getAllDataPageAbleIsActive(int firstResult, int maxResults, Order order, Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Kecamatan> getAllDataPageAbleIsActive(int firstResult, int maxResults, Order order, Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.SUPPORTS, timeout = 50)
    public List<Kecamatan> getByName(String name) throws Exception {
        return kecamatanDao.getByName(name);
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.SUPPORTS, timeout = 50)
    public List<Kecamatan> getByKabupatenId(long kabupatenId) throws Exception {
        return kecamatanDao.getByKabupatenId(kabupatenId);
    }

}
