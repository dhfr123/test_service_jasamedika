/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.service.impl;

import com.dhfr.common.util.HashingUtils;
import com.dhfr.data.core.service.impl.IServiceImpl;
import com.dhfr.jasamedika.dao.SpiUserDao;
import com.dhfr.jasamedika.dao.SpiUserRoleDao;
import com.dhfr.jasamedika.entity.SpiUser;
import com.dhfr.jasamedika.entity.SpiUserRole;
import com.dhfr.jasamedika.exception.BussinessException;
import com.dhfr.jasamedika.service.SpiUserService;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author denifahri
 */
@Service(value = "spiUserService")
@Lazy
public class SpiUserServiceImpl extends IServiceImpl implements SpiUserService {
    
    @Autowired
    private SpiUserDao spiUserDao;
    @Autowired
    private SpiUserRoleDao spiUserRoleDao;
    
    @Override
    public SpiUser getEntiyByPK(String id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser getEntiyByPK(Integer id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, timeout = 30)
    public SpiUser getEntiyByPK(Long id) throws Exception {
        return this.spiUserDao.getEntiyByPK(id);
    }
    
    @Override
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void save(SpiUser entity) throws Exception {
        long totalDuplicatesuserId = spiUserDao.getTotalByUserId(entity.getUserId());
        long totalDuplicateEmail = spiUserDao.getTotalByEmailAddress(entity.getEmailAddress());
        long totalDuplicatePhone = spiUserDao.getTotalByPhone(entity.getPhoneNumber());
        if (totalDuplicatesuserId > 0) {
            throw new BussinessException("entity_userId.errror_duplicate");
        }
        if (totalDuplicateEmail > 0) {
            throw new BussinessException("entity_email.errror_duplicate");
        }
        if (totalDuplicatePhone > 0) {
            throw new BussinessException("entity_phone.errror_duplicate");
        }
        
        entity.setPassword(HashingUtils.getHashSHA256(entity.getPassword()));
        entity.setCreatedOn(new Date());
        spiUserDao.save(entity);
    }
    
    @Override
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void update(SpiUser entity) throws Exception {
        long totalDuplicatesuserId = spiUserDao.getTotalByUserIdAndNotId(entity.getUserId(), entity.getId());
        long totalDuplicateEmail = spiUserDao.getTotalByEmailAndNotId(entity.getEmailAddress(), entity.getId());
        long totalDuplicatePhone = spiUserDao.getTotalByPoneAndNotID(entity.getPhoneNumber(), entity.getId());
        if (totalDuplicatesuserId > 0) {
            throw new BussinessException("entity_userId.errror_duplicate");
        }
        if (totalDuplicateEmail > 0) {
            throw new BussinessException("entity_email.errror_duplicate");
        }
        if (totalDuplicatePhone > 0) {
            throw new BussinessException("entity_phone.errror_duplicate");
        }
        SpiUser spiUser = spiUserDao.getEntiyByPK(entity.getId());
        spiUser.getSpiUserRoles().clear();
        spiUser.setEmailAddress(entity.getEmailAddress());
        spiUser.setIsActive(entity.getIsActive());
        spiUser.setIsExpired(entity.getIsExpired());
        spiUser.setIsLock(entity.getIsLock());
//        spiUser.setPassword(HashingUtils.getHashSHA256(entity.getPassword())); 
//        Tidak di set di sini karena diimplemeasti dengan bussine proses berbeda,
//        yang dan update password user bersangkutan atau di reset berdasarkan requst user tersebut.
        spiUser.setPhoneNumber(entity.getPhoneNumber());
        spiUser.setRealName(entity.getRealName());
        spiUser.setUpdatedOn(new Date());
        spiUser.setUpdatedBy(entity.getUpdatedBy());
        spiUserDao.updateAndMerge(spiUser);
        Set<SpiUserRole> dataToSave = entity.getSpiUserRoles();
        dataToSave.stream().map((spiUserRole) -> {
            spiUserRole.setSpiUser(spiUser);
            return spiUserRole;
        }).forEach((spiUserRole) -> {
            this.spiUserRoleDao.save(spiUserRole);
        });
        
    }
    
    @Override
    public void saveOrUpdate(SpiUser enntity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser saveData(SpiUser entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser updateData(SpiUser entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser saveOrUpdateData(SpiUser entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser getEntityByPkIsActive(String id, Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser getEntityByPkIsActive(String id, Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser getEntityByPkIsActive(String id, Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser getEntityByPkIsActive(Integer id, Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser getEntityByPkIsActive(Integer id, Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser getEntityByPkIsActive(Integer id, Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser getEntityByPkIsActive(Long id, Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser getEntityByPkIsActive(Long id, Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SpiUser getEntityByPkIsActive(Long id, Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete(SpiUser entity) throws Exception {
        this.spiUserDao.delete(entity);
    }
    
    @Override
    public void softDelete(SpiUser entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, timeout = 30)
    public Long getTotalData() throws Exception {
        return this.spiUserDao.getTotalData();
    }
    
    @Override
    public Long getTotalDataIsActive(Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Long getTotalDataIsActive(Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Long getTotalDataIsActive(Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<SpiUser> getAllData() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<SpiUser> getAllData(Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<SpiUser> getAllData(Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<SpiUser> getAllData(Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, timeout = 50)
    public List<SpiUser> getAllDataPageAble(int firstResult, int maxResults, Order order) throws Exception {
        return this.spiUserDao.getAllDataPageAble(firstResult, maxResults, order);
    }
    
    @Override
    public List<SpiUser> getAllDataPageAbleIsActive(int firstResult, int maxResults, Order order, Boolean isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<SpiUser> getAllDataPageAbleIsActive(int firstResult, int maxResults, Order order, Integer isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<SpiUser> getAllDataPageAbleIsActive(int firstResult, int maxResults, Order order, Byte isActive) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, timeout = 30)
    public SpiUser getByIdWithDetail(Long id) throws Exception {
        return this.spiUserDao.getByIdWithDetail(id);
    }
    
}
