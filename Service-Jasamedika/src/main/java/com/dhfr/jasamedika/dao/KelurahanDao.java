/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao;

import com.dhfr.data.core.dao.IDAO;
import com.dhfr.jasamedika.entity.Kelurahan;
import java.util.List;

/**
 *
 * @author denifahri
 */
public interface KelurahanDao extends IDAO<Kelurahan> {

    public List<Kelurahan> getByName(String name);

    public List<Kelurahan> getBykecamatanId(Long id);

}
