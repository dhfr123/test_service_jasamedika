/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao.impl;

import com.dhfr.data.core.dao.impl.IDAOImpl;
import com.dhfr.jasamedika.dao.KabupatenKotaDao;
import com.dhfr.jasamedika.entity.KabupatenKota;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

/**
 *
 * @author denifahri
 */
@Repository(value = "kabupatenKotaDao")
@Lazy
public class KabupatenKotaDaoImpl extends IDAOImpl<KabupatenKota> implements KabupatenKotaDao {

    @Override
    public Class<KabupatenKota> getEntityClass() {
        return KabupatenKota.class;
    }

    @Override
    public List<KabupatenKota> getByName(String kabupatenName) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.like("name", kabupatenName, MatchMode.ANYWHERE));
        return criteria.list();
    }

    @Override
    public List<KabupatenKota> getByProvinceId(Long provinceId) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.createAlias("province", "province", JoinType.INNER_JOIN);
        criteria.add(Restrictions.eq("province.id", provinceId));
        return criteria.list();
    }

}
