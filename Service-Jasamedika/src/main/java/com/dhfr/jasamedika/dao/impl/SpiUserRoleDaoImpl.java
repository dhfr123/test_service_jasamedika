/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao.impl;

import com.dhfr.data.core.dao.impl.IDAOImpl;
import com.dhfr.jasamedika.dao.SpiUserRoleDao;
import com.dhfr.jasamedika.entity.SpiUserRole;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

/**
 *
 * @author denifahri
 */
@Repository(value = "spiUserRoleDao")
@Lazy
public class SpiUserRoleDaoImpl extends IDAOImpl<SpiUserRole> implements SpiUserRoleDao {

    @Override
    public Class<SpiUserRole> getEntityClass() {
        return SpiUserRole.class;
    }

}
