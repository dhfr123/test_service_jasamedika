/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao.impl;

import com.dhfr.data.core.dao.impl.IDAOImpl;
import com.dhfr.jasamedika.dao.KelurahanDao;
import com.dhfr.jasamedika.entity.Kelurahan;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

/**
 *
 * @author denifahri
 */
@Repository(value = "kelurahanDao")
@Lazy
public class KelurahanDaoImpl extends IDAOImpl<Kelurahan> implements KelurahanDao {

    @Override
    public Class<Kelurahan> getEntityClass() {
        return Kelurahan.class;
    }

    @Override
    public List<Kelurahan> getByName(String name) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.like("name", name, MatchMode.ANYWHERE));
        return criteria.list();
    }

    @Override
    public List<Kelurahan> getBykecamatanId(Long id) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.createAlias("kecamatan", "kecamatan", JoinType.INNER_JOIN);
        criteria.add(Restrictions.eq("kecamatan.id", id));
        return criteria.list();
    }

}
