/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao.impl;

import com.dhfr.data.core.dao.impl.IDAOImpl;
import com.dhfr.jasamedika.dao.ProvinceDao;
import com.dhfr.jasamedika.entity.Province;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

/**
 *
 * @author denifahri
 */
@Repository(value = "provinceDao")
@Lazy
public class ProvinceDaoImpl extends IDAOImpl<Province> implements ProvinceDao {

    @Override
    public Class<Province> getEntityClass() {
        return Province.class;
    }

    @Override
    public List<Province> getByprovinceName(String provinceName) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.like("name", provinceName, MatchMode.ANYWHERE));
        return criteria.list();
    }

    @Override
    public List<Province> getByContryId(Long countryId) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.createAlias("country", "country");
        criteria.add(Restrictions.eq("country.id", countryId));
        return criteria.list();
    }

}
