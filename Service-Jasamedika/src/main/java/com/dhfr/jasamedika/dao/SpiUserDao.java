/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao;

import com.dhfr.data.core.dao.IDAO;
import com.dhfr.jasamedika.entity.SpiUser;

/**
 *
 * @author denifahri
 */
public interface SpiUserDao extends IDAO<SpiUser> {

    public Long getTotalByUserId(String userId);

    public Long getTotalByUserIdAndNotId(String userId, Long id);

    public Long getTotalByEmailAddress(String emailAddress);

    public Long getTotalByEmailAndNotId(String emailAddress, Long id);

    public Long getTotalByPhone(String phone);

    public Long getTotalByPoneAndNotID(String phone, Long id);

    public void updateAndMerge(SpiUser spiUser);

    public SpiUser getByIdWithDetail(Long id);

}
