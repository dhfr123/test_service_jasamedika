/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao;

import com.dhfr.data.core.dao.IDAO;
import com.dhfr.jasamedika.entity.Country;
import java.util.List;

/**
 *
 * @author denifahri
 */
public interface CountryDao extends IDAO<Country> {

    public Long getTotalByPhone(String phone);

    public Long getTotalByPhhoneAndNotId(String phone, Long id);

    public List<Country> getByCountryName(String name);
}
