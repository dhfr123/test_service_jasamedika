/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao;

import com.dhfr.data.core.dao.IDAO;
import com.dhfr.jasamedika.entity.Pasien;

/**
 *
 * @author denifahri
 */
public interface PasienDao extends IDAO<Pasien> {

    public Long getTotalByPasienId(String pasienId);

    public Long getTotalByPasienIdAndNotId(String pasienId, Long id);
    
    public Pasien getByMaxCreateDate();
    
    public Pasien getByPasienIdWithDetail(String pasienId);
}
