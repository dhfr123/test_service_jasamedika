/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao;

import com.dhfr.data.core.dao.IDAO;
import com.dhfr.jasamedika.entity.Kecamatan;
import java.util.List;

/**
 *
 * @author denifahri
 */
public interface KecamatanDao extends IDAO<Kecamatan> {

    public List<Kecamatan> getByName(String name);

    public List<Kecamatan> getByKabupatenId(long kabupatenId);
}
