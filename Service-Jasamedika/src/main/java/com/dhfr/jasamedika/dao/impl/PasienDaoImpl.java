/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao.impl;

import com.dhfr.data.core.dao.impl.IDAOImpl;
import com.dhfr.jasamedika.dao.PasienDao;
import com.dhfr.jasamedika.entity.Pasien;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

/**
 *
 * @author denifahri
 */
@Repository(value = "pasienDao")
@Lazy
public class PasienDaoImpl extends IDAOImpl<Pasien> implements PasienDao {

    @Override
    public Class<Pasien> getEntityClass() {
        return Pasien.class;
    }

    @Override
    public Long getTotalByPasienId(String pasienId) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.eq("pasienId", pasienId));
        return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
    }

    @Override
    public Long getTotalByPasienIdAndNotId(String pasienId, Long id) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.eq("pasienId", pasienId));
        criteria.add(Restrictions.ne("id", id));
        return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
    }

    @Override
    public Pasien getByMaxCreateDate() {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        ProjectionList proList = Projections.projectionList();
        proList.add(Property.forName("createdOn").max());
        DetachedCriteria maxCreateDate = DetachedCriteria.forClass(getEntityClass())
                .setProjection(proList);
        String[] var = {"createdOn"};
        criteria.add(Subqueries.propertiesIn(var, maxCreateDate));
        return (Pasien) criteria.uniqueResult();

    }

    @Override
    public Pasien getByPasienIdWithDetail(String pasienId) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.eq("pasienId", pasienId));
        criteria.setFetchMode("kelurahan", FetchMode.JOIN);
        criteria.setFetchMode("kelurahan.kecamatan", FetchMode.JOIN);
        criteria.setFetchMode("kelurahan.kecamatan.kabupatenKota", FetchMode.JOIN);
        criteria.setFetchMode("kelurahan.kecamatan.kabupatenKota.province", FetchMode.JOIN);
        return (Pasien) criteria.uniqueResult();

    }

}
