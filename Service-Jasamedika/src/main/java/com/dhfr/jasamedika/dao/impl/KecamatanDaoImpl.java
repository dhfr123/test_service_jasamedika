/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao.impl;

import com.dhfr.data.core.dao.impl.IDAOImpl;
import com.dhfr.jasamedika.dao.KecamatanDao;
import com.dhfr.jasamedika.entity.Kecamatan;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

/**
 *
 * @author denifahri
 */
@Repository(value = "kecamatanDao")
@Lazy
public class KecamatanDaoImpl extends IDAOImpl<Kecamatan> implements KecamatanDao{

    @Override
    public Class<Kecamatan> getEntityClass() {
       return Kecamatan.class;
    }

    @Override
    public List<Kecamatan> getByName(String name) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.like("name", name, MatchMode.ANYWHERE));
        return criteria.list();
    }

    @Override
    public List<Kecamatan> getByKabupatenId(long kabupatenId) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.createAlias("kabupatenKota", "kabupatenKota", JoinType.INNER_JOIN);
        criteria.add(Restrictions.eq("kabupatenKota.id", kabupatenId));
        return criteria.list();
    }
    
}
