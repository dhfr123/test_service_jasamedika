/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao.impl;

import com.dhfr.data.core.dao.impl.IDAOImpl;
import com.dhfr.jasamedika.dao.SpiRoleDao;
import com.dhfr.jasamedika.entity.SpiRole;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

/**
 *
 * @author denifahri
 */
@Repository(value = "spiRoleDao")
@Lazy
public class SpiRoleDaoImpl extends IDAOImpl<SpiRole> implements SpiRoleDao {

    @Override
    public Class<SpiRole> getEntityClass() {
        return SpiRole.class;
    }

}
