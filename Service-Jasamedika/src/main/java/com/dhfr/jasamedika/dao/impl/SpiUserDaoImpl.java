/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhfr.jasamedika.dao.impl;

import com.dhfr.data.core.dao.impl.IDAOImpl;
import com.dhfr.jasamedika.dao.SpiUserDao;
import com.dhfr.jasamedika.entity.SpiUser;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

/**
 *
 * @author denifahri
 */
@Repository(value = "spiUserDao")
@Lazy
public class SpiUserDaoImpl extends IDAOImpl<SpiUser> implements SpiUserDao {

    @Override
    public Class<SpiUser> getEntityClass() {
        return SpiUser.class;
    }

    @Override
    public Long getTotalByUserId(String userId) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.eq("userId", userId));
        return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
    }

    @Override
    public Long getTotalByUserIdAndNotId(String userId, Long id) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.eq("userId", userId));
        criteria.add(Restrictions.ne("id", id));
        return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
    }

    @Override
    public Long getTotalByEmailAddress(String emailAddress) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.eq("emailAddress", emailAddress));
        return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();

    }

    @Override
    public Long getTotalByEmailAndNotId(String emailAddress, Long id) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.eq("emailAddress", emailAddress));
        criteria.add(Restrictions.ne("id", id));
        return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
    }

    @Override
    public Long getTotalByPhone(String phone) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.eq("phoneNumber", phone));
        return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();

    }

    @Override
    public Long getTotalByPoneAndNotID(String phone, Long id) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.eq("phoneNumber", phone));
        criteria.add(Restrictions.ne("id", id));
        return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
    }

    @Override
    public void updateAndMerge(SpiUser spiUser) {
        getCurrentSession().update(spiUser);
        getCurrentSession().flush();
    }

    @Override
    public SpiUser getByIdWithDetail(Long id) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass());
        criteria.add(Restrictions.eq("id", id));
        criteria.setFetchMode("spiUserRoles", FetchMode.JOIN);
        criteria.setFetchMode("spiUserRoles.spiRole", FetchMode.JOIN);
        return (SpiUser) criteria.uniqueResult();
    }

}
