package com.dhfr.jasamedika.entity;
// Generated Nov 25, 2016 7:23:16 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 * Kelurahan generated by hbm2java
 */
@Entity
@Table(name="kelurahan"
    ,catalog="sik"
    , uniqueConstraints = @UniqueConstraint(columnNames="code") 
)
public class Kelurahan  implements java.io.Serializable {


     private long id;
     private Integer version;
     private Kecamatan kecamatan;
     private String createdBy;
     private Date createdOn;
     private String code;
     private String name;
     private String nameKota;
     private String updatedBy;
     private Date updatedOn;
     private String description;
     private Set<Pasien> pasiens = new HashSet<Pasien>(0);

    public Kelurahan() {
    }

	
    public Kelurahan(long id) {
        this.id = id;
    }
    public Kelurahan(long id, Kecamatan kecamatan, String createdBy, Date createdOn, String code, String name, String nameKota, String updatedBy, Date updatedOn, String description, Set<Pasien> pasiens) {
       this.id = id;
       this.kecamatan = kecamatan;
       this.createdBy = createdBy;
       this.createdOn = createdOn;
       this.code = code;
       this.name = name;
       this.nameKota = nameKota;
       this.updatedBy = updatedBy;
       this.updatedOn = updatedOn;
       this.description = description;
       this.pasiens = pasiens;
    }
   
     @Id 

    
    @Column(name="id", unique=true, nullable=false)
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
        this.id = id;
    }

    @Version
    @Column(name="version")
    public Integer getVersion() {
        return this.version;
    }
    
    public void setVersion(Integer version) {
        this.version = version;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="kecamatan_id")
    public Kecamatan getKecamatan() {
        return this.kecamatan;
    }
    
    public void setKecamatan(Kecamatan kecamatan) {
        this.kecamatan = kecamatan;
    }

    
    @Column(name="created_by", length=45)
    public String getCreatedBy() {
        return this.createdBy;
    }
    
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_on", length=19)
    public Date getCreatedOn() {
        return this.createdOn;
    }
    
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    
    @Column(name="code", unique=true, length=8)
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

    
    @Column(name="name", length=80)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="name_kota", length=80)
    public String getNameKota() {
        return this.nameKota;
    }
    
    public void setNameKota(String nameKota) {
        this.nameKota = nameKota;
    }

    
    @Column(name="updated_by", length=45)
    public String getUpdatedBy() {
        return this.updatedBy;
    }
    
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_on", length=19)
    public Date getUpdatedOn() {
        return this.updatedOn;
    }
    
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    
    @Column(name="description", length=65535)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="kelurahan")
    public Set<Pasien> getPasiens() {
        return this.pasiens;
    }
    
    public void setPasiens(Set<Pasien> pasiens) {
        this.pasiens = pasiens;
    }




}


