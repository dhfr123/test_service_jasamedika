package com.dhfr.jasamedika.entity;
// Generated Nov 25, 2016 7:23:16 PM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * SpiUserRoleId generated by hbm2java
 */
@Embeddable
public class SpiUserRoleId  implements java.io.Serializable {


     private long userId;
     private long roleId;

    public SpiUserRoleId() {
    }

    public SpiUserRoleId(long userId, long roleId) {
       this.userId = userId;
       this.roleId = roleId;
    }
   


    @Column(name="user_id", nullable=false)
    public long getUserId() {
        return this.userId;
    }
    
    public void setUserId(long userId) {
        this.userId = userId;
    }


    @Column(name="role_id", nullable=false)
    public long getRoleId() {
        return this.roleId;
    }
    
    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof SpiUserRoleId) ) return false;
		 SpiUserRoleId castOther = ( SpiUserRoleId ) other; 
         
		 return (this.getUserId()==castOther.getUserId())
 && (this.getRoleId()==castOther.getRoleId());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + (int) this.getUserId();
         result = 37 * result + (int) this.getRoleId();
         return result;
   }   


}


