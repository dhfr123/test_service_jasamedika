/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author denifahri
 */
import com.dhfr.common.util.RandomNumberUtil;
import com.dhfr.jasamedika.entity.Country;
import com.dhfr.jasamedika.entity.KabupatenKota;
import com.dhfr.jasamedika.entity.Kecamatan;
import com.dhfr.jasamedika.entity.Kelurahan;
import com.dhfr.jasamedika.entity.Pasien;
import com.dhfr.jasamedika.entity.Province;
import com.dhfr.jasamedika.entity.SpiRole;
import com.dhfr.jasamedika.entity.SpiUser;
import com.dhfr.jasamedika.entity.SpiUserRole;
import com.dhfr.jasamedika.entity.SpiUserRoleId;
import com.dhfr.jasamedika.service.CountryService;
import com.dhfr.jasamedika.service.KabupatenKotaService;
import com.dhfr.jasamedika.service.KecamatanService;
import com.dhfr.jasamedika.service.KelurahanService;
import com.dhfr.jasamedika.service.PasienService;
import com.dhfr.jasamedika.service.ProvinceService;
import com.dhfr.jasamedika.service.SpiRoleService;
import com.dhfr.jasamedika.service.SpiUserRoleService;
import com.dhfr.jasamedika.service.SpiUserService;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Order;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ContextConfiguration(locations = {
    "classpath:spring/dataCore.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CRUDTest {

    protected transient Logger LOGGER = Logger.getLogger(getClass());
    @Autowired
    private SpiRoleService spiRoleService;
    @Autowired
    private SpiUserService spiUserService;
    @Autowired
    private SpiUserRoleService spiUserRoleService;
    @Autowired
    private PasienService pasienService;
    @Autowired
    private CountryService countryService;
    @Autowired
    private ProvinceService provinceService;
    @Autowired
    private KabupatenKotaService kabupatenKotaService;
    @Autowired
    private KecamatanService kecamatanService;
    @Autowired
    private KelurahanService kelurahanService;

    @Test
    public void testSpiRole() {
        try {
//        Test Save
            SpiRole spiRole1 = new SpiRole();
            spiRole1.setName("Admin 1");
            spiRole1.setDescription("OKKKK");

            spiRoleService.save(spiRole1);

            SpiRole spiRole2 = new SpiRole();
            spiRole2.setName("Admin 2");
            spiRole2.setDescription("OKKKK");
            spiRoleService.save(spiRole2);

            //Test save Get All with pagging adn getTotal Data
            List<SpiRole> pageData = spiRoleService.getAllDataPageAble(0, 10, Order.asc("id"));
            Long totalData = spiRoleService.getTotalData();
            // Asumsi jika tidak ada data lama di database
            Assert.assertEquals(totalData.intValue(), pageData.size());

//        Test Update and getById or PK and Delete
            for (SpiRole spiRole : pageData) {

                spiRole.setName("Admin Update" + spiRole.getName());
                spiRole.setDescription("Ok Update");
                spiRoleService.update(spiRole);
                SpiRole foreEvaluate = spiRoleService.getEntiyByPK(spiRole.getId());
                Assert.assertEquals(foreEvaluate.getId(), spiRole.getId());
                Assert.assertEquals(foreEvaluate.getName(), spiRole.getName());
                Assert.assertEquals(foreEvaluate.getDescription(), spiRole.getDescription());
                spiRoleService.delete(foreEvaluate);
            }

        } catch (Exception ex) {
            LOGGER.error(ex, ex);
        }
    }

    @Test
    public void testSpiUser() {

        try {

            SpiRole spiRole1 = new SpiRole();
            spiRole1.setName("Admin 1");
            spiRole1.setDescription("OKKKK");
            spiRoleService.save(spiRole1);

            SpiRole spiRole2 = new SpiRole();
            spiRole2.setName("Admin 2");
            spiRole2.setDescription("OKKKK");
            spiRoleService.save(spiRole2);

            SpiUser spiUser = new SpiUser();
            spiUser.setId(Long.parseLong(RandomNumberUtil.getRandomNumber(9)));
            spiUser.setUserId("deni.fahri");
            spiUser.setCreatedBy("Admin");
            spiUser.setEmailAddress("dddd@dsdsfds.com");
            spiUser.setIsActive(Boolean.TRUE);
            spiUser.setIsLock(Boolean.FALSE);
            spiUser.setIsExpired(Boolean.FALSE);
            spiUser.setPassword("dhfr@123");
            spiUser.setPhoneNumber("+628232323");
            spiUser.setRealName("Deni Husni FR");
            List<SpiRole> pageData = spiRoleService.getAllDataPageAble(0, 10, Order.asc("id"));
            Set<SpiUserRole> dataToInclude = new HashSet<>();
            pageData.stream().map((spiRole) -> {
                SpiUserRole spiUserRole = new SpiUserRole();
                spiUserRole.setId(new SpiUserRoleId(spiUser.getId(), spiRole.getId()));
                spiUserRole.setSpiRole(spiRole);
                return spiUserRole;
            }).map((spiUserRole) -> {
                spiUserRole.setSpiUser(spiUser);
                return spiUserRole;
            }).forEach((spiUserRole) -> {
                dataToInclude.add(spiUserRole);
            });
            spiUser.setSpiUserRoles(dataToInclude);
            spiUserService.save(spiUser);

            SpiUser spiUser1 = new SpiUser();
            spiUser1.setId(Long.parseLong(RandomNumberUtil.getRandomNumber(9)));
            spiUser1.setUserId("deni.husni");
            spiUser1.setCreatedBy("Admin");
            spiUser1.setEmailAddress("dddd1@dsdsfds.com");
            spiUser1.setIsActive(Boolean.TRUE);
            spiUser1.setIsLock(Boolean.FALSE);
            spiUser1.setIsExpired(Boolean.FALSE);
            spiUser1.setPassword("dhfr@123");
            spiUser1.setPhoneNumber("+62823232553");
            spiUser1.setRealName("Deni Husni Ok");
            List<SpiRole> pageData1 = spiRoleService.getAllDataPageAble(0, 10, Order.asc("id"));
            Set<SpiUserRole> dataToInclude1 = new HashSet<>();
            pageData1.stream().map((spiRole) -> {
                SpiUserRole spiUserRole = new SpiUserRole();
                spiUserRole.setId(new SpiUserRoleId(spiUser1.getId(), spiRole.getId()));
                spiUserRole.setSpiRole(spiRole);
                return spiUserRole;
            }).map((spiUserRole) -> {
                spiUserRole.setSpiUser(spiUser);
                return spiUserRole;
            }).forEach((spiUserRole) -> {
                dataToInclude1.add(spiUserRole);
            });
            spiUser1.setSpiUserRoles(dataToInclude1);
            spiUserService.save(spiUser1);
//Test save, Get All with pagging adn getTotal Data
            List<SpiUser> pageDataUser = spiUserService.getAllDataPageAble(0, 10, Order.asc("id"));
            Long totalData = spiUserService.getTotalData();

//            Asumsi tidak ada data lama di server
            Assert.assertEquals(totalData.intValue(), pageDataUser.size());
//           Test Update and getById or PK and Delete
            for (SpiUser spiUser2 : pageDataUser) {
                spiUser2.setRealName("Update Admin " + spiUser2.getRealName());
                spiUser2.setIsExpired(Boolean.TRUE);
                Set<SpiUserRole> forUpdate = new HashSet<>();
                SpiUserRole spiUserRole = new SpiUserRole();
                spiUserRole.setId(new SpiUserRoleId(spiUser2.getId(), pageData.get(0).getId()));
                spiUserRole.setSpiRole(pageData.get(0));
                spiUserRole.setSpiUser(spiUser2);
                forUpdate.add(spiUserRole);
                spiUser2.setSpiUserRoles(forUpdate);
                spiUserService.update(spiUser2);
                SpiUser forEvaluate = spiUserService.getByIdWithDetail(spiUser2.getId());
                Assert.assertEquals(forEvaluate.getId(), spiUser2.getId());
                Assert.assertEquals(forEvaluate.getRealName(), spiUser2.getRealName());
                Assert.assertEquals(forEvaluate.getSpiUserRoles().size(), forUpdate.size());
                List<SpiUserRole> forCheck = new ArrayList<>(forEvaluate.getSpiUserRoles());
                Assert.assertEquals(forCheck.get(0).getId(), spiUserRole.getId());
                spiUserService.delete(forEvaluate);

            }
            Long totalDataUpterDeletUser = spiUserService.getTotalData();
            Long totalSpiUserRoleUpetDelete = spiUserRoleService.getTotalData();
            Assert.assertEquals(totalDataUpterDeletUser.intValue(), totalSpiUserRoleUpetDelete.intValue());

            List<SpiRole> pageDataForDelete = spiRoleService.getAllDataPageAble(0, 10, Order.asc("id"));
            for (SpiRole spiRole : pageDataForDelete) {
                spiRoleService.delete(spiRole);
            }
            Long totalDataUpterDelte = spiRoleService.getTotalData();
            Assert.assertEquals(0, totalDataUpterDelte.intValue());
        } catch (Exception ex) {
            LOGGER.error(ex, ex);
        }
    }

//
//    @Test
//    public void testCountry() {
//        try {
//            Country country = new Country();
//            country.setCode("AUS");
//            country.setCreatedBy("deni.fahri");
//            country.setName("Aistralia");
//            countryService.save(country);
//        } catch (Exception ex) {
//            LOGGER.error(ex, ex);
//        }
//    }
//    @Test
//    public void testProvince() {
//        try {
//            Province province = new Province();
//            province.setCode("JABAR");
//            province.setName("Jawa Batar");
//            province.setCreatedBy("deni.fahri");
//            province.setCountry(new Country(1l));
//            provinceService.save(province);
//        } catch (Exception ex) {
//            LOGGER.error(ex, ex);
//        }
//    }
//    @Test
//    public void testKabupaten() {
//        try {
//            KabupatenKota kabupatenKota = new KabupatenKota();
//            kabupatenKota.setCode("KABBAB");
//            kabupatenKota.setName("Kabupaten Bandung");
//            kabupatenKota.setCreatedBy("deni.fahri");
//            kabupatenKota.setProvince(new Province(1l));
//            kabupatenKotaService.save(kabupatenKota);
//        } catch (Exception ex) {
//            LOGGER.error(ex, ex);
//        }
//    }
//    @Test
//    public void testKecamatan() {
//        try {
//            Kecamatan kecamatan = new Kecamatan();
//            kecamatan.setKabupatenKota(new KabupatenKota(1l));
//            kecamatan.setCode("PMPK");
//            kecamatan.setName("Pameungpeuk");
//            kecamatan.setCreatedBy("deni.fahri");
//            kecamatanService.save(kecamatan);
//        } catch (Exception ex) {
//            LOGGER.error(ex, ex);
//        }
//    }
//    @Test
//    public void testKelurahan() {
//        try {
//            Kelurahan kelurahan = new Kelurahan();
//            kelurahan.setCode("RCMLY");
//            kelurahan.setName("Rancamulya");
//            kelurahan.setNameKota("Bandung");
//            kelurahan.setKecamatan(new Kecamatan(1l));
//            kelurahan.setCreatedBy("deni.fahri");
//            kelurahanService.save(kelurahan);
//        } catch (Exception ex) {
//            LOGGER.error(ex, ex);
//        }
//
//    }
    @Test
    public void testPasien() {
        try {
//            Pasien pasien = new Pasien();
//            pasien.setName("Aaa Sakit");
//            pasien.setGender("L");
//            pasien.setTglLahir(new Date());
//            pasien.setAlamat("Kampung  Rancaengan No. 3");
//            pasien.setPhoneNumber("+623343434");
//            pasien.setRt(1);
//            pasien.setRw(2);
//            pasien.setKelurahan(new Kelurahan(1l));
//            pasien.setCreatedBy("deni.fahri");
//            pasienService.save(pasien);
//        } catch (Exception ex) {
//            LOGGER.error(ex, ex);
//        }
            Pasien pasien = new Pasien();
            pasien.setName("Test Orang Sakit");
            pasien.setGender("L");
            pasien.setTglLahir(new Date());
            pasien.setAlamat("Kampung  Rancaengan No. 3");
            pasien.setPhoneNumber("+623343434");
            pasien.setRt(1);
            pasien.setRw(2);
            pasien.setKelurahan(new Kelurahan(1l));
            pasien.setCreatedBy("deni.fahri");
            Pasien hasilResigistrasi = pasienService.registrasi(pasien);
            Assert.assertEquals(pasien.getName(), hasilResigistrasi.getName());
            Assert.assertNotNull(hasilResigistrasi.getPasienId());
            LOGGER.info(" Registrasi Sukses. Pisen ID :" + hasilResigistrasi.getPasienId());
        } catch (Exception ex) {
            LOGGER.error(ex, ex);
        }
    }
}
